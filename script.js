

$(document).ready(function () {
  loadComment()
  $("#form-comments").on("submit", onSubmit);



  $(".reply").click(function (event) {
    event.preventDefault();
    console.log($(this).parent().parent().parent().attr('id'));

    $(this).parent().append(`
      <form class="form-inner-comments" action="" method="PUT">
      <div class="form-comments">
          <label for="name">Name: </label>
          <input type="text" name="name" id="name" required>
        </div>
        <div class="form-example">
          <label for="email">Subject: </label>
          <input type="text" name="subject" id="subject" required>
        </div>

        <div class="form-comments">
          <textarea name="textarea" rows="3" cols="50"></textarea>
        </div>
        <div class="form-comments">
        <input type="radio"
          id="red"
          name="colour"
          value="red"
          checked>
        <svg height="100"
          width="100">
          <circle cx="50"
            cy="50"
            r="20"
            fill="red">
        </svg>
      </div>
      <div class="colour-radio">
        <div class="form-comments">
          <input type="radio"
            id="green"
            name="colour"
            value="green">
          <svg height="100"
            width="100">
            <circle cx="50"
              cy="50"
              r="20"
              fill="green">
          </svg>
        </div>
        <div class="form-comments">
          <input type="radio"
            id="blue"
            name="colour"
            value="blue">
          <svg height="100"
            width="100">
            <circle cx="50"
              cy="50"
              r="20"
              fill="blue">
          </svg> 
        </div>
        <div class="form-comments">
          <input type="radio"
            id="yellow"
            name="colour"
            value="yellow">
          <svg height="100"
            width="100">
            <circle cx="50"
              cy="50"
              r="20"
              fill="yellow">
          </svg> 
        </div>
      </div>
      <div class="form-comments">
        <input type="submit" value="Reply!">
      </div>
  </form>
      `)
    $(".form-inner-comments").on("submit", onReply);


  });
});

function getDateTime() {
  var dt = new Date($.now())
  var year = dt.getFullYear()
  var month = dt.getMonth() + 1
  var date = dt.getDate()
  var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
  return `${year}-${month}-${date} ${time}`
}

function getData(ajaxurl) { 
  return $.getJSON({
    url: ajaxurl,
    type: 'GET',
  });
};

async function onSubmit(event) {
  event.preventDefault();
  var formDataArray = $(this).serializeArray()
  var dateTime = getDateTime()
  var data = await getData('http://gd.geobytes.com/GetCityDetails?callback=?')
  console.log(JSON.stringify(data, null, 2));
  var text = `

      <li class="media">
        <svg height="100" width="100">
          <circle cx="50" cy="50" r="40" fill="${formDataArray[3].value}">
        </svg>
        <div class="media-body">
          <h5>${formDataArray[1].value}</h5>
          <h6>${formDataArray[0].value}</h6>
          <p>${formDataArray[2].value}</p>
          <p>Updated at: ${dateTime}</p>
          <p>IP Address: ${data.geobytesipaddress}</p>
          <p>Location: ${data.geobytescountry}</p>
          <p>LAT: ${data.geobyteslatitude}</p>
          <p>Long: ${data.geobyteslongitude}</p>

          <a  href="" class="reply">Reply</a>
        </div>
      </li>

    `
  $('#comments').append(text)
  var text = $('#comments').html()
  $.ajax({
    url: 'data.txt',
    type: 'put',
    dataType: 'text/plain',
    data: text,
    success: function (data) {
      // ... do something with the data...
    }
  }).always(function () {
    loadComment()
  });
  $(this).trigger("reset");
}

async function onReply(event) {
  event.preventDefault();
  var formDataArray = $(this).serializeArray()
  var dateTime = getDateTime()
  var data = await getData('http://gd.geobytes.com/GetCityDetails?callback=?')
  console.log($(this).serializeArray());
  var text = `
    <div class="container m-5">
    <ul id="comments" class="list-unstyled">
      <li class="media">
        <svg height="100" width="100">
          <circle cx="50" cy="50" r="40" fill="${formDataArray[3].value}">
        </svg>
        <div class="media-body">
          <h5>${formDataArray[1].value}</h5>
          <h6>${formDataArray[0].value}</h6>
          <p>${formDataArray[2].value}</p>
          <p>Updated at: ${dateTime}</p>
          <p>IP Address: ${data.geobytesipaddress}</p>
          <p>Location: ${data.geobytescountry}</p>
          <p>LAT: ${data.geobyteslatitude}</p>
          <p>Long: ${data.geobyteslongitude}</p>
        </div>
      </li>
    </ul>
    </div>
    `
  $(this).parent().append(text)
  $(".form-inner-comments").css("display", "none")
}

function loadComment() {
  $.ajax({
    url: 'data.txt',
    type: 'get',
    success: function (data) {
      // ... do something with the data...
      if (data) {

        $('#comments').html(data)
        $(".reply").click(function (event) {
          event.preventDefault();
          console.log($(this).parent().parent().parent().attr('id'));

          $(this).parent().append(`
                    <form class="form-inner-comments" action="" method="PUT">
                    <div class="form-comments">
                      <label for="name">Name: </label>
                      <input type="text" name="name" id="name" required>
                    </div>
                    <div class="form-comments">
                      <label for="email">Subject: </label>
                      <input type="text" name="subject" id="subject" required>
                    </div>
                    <div class="form-comments">
                      <label for="comment">Your Comment: </label>

                      <textarea name="textarea" rows="3" cols="50"></textarea>
                    </div>
                    <div class="colour-radio">
                      <div class="form-comments">
                        <input type="radio"
                          id="red"
                          name="colour"
                          value="red"
                          checked>
                        <svg height="100"
                          width="100">
                          <circle cx="50"
                            cy="50"
                            r="20"
                            fill="red">
                        </svg>
                      </div>
                      <div class="form-comments">
                        <input type="radio"
                          id="green"
                          name="colour"
                          value="green">
                        <svg height="100"
                          width="100">
                          <circle cx="50"
                            cy="50"
                            r="20"
                            fill="green">
                        </svg>
                      </div>
                      <div class="form-comments">
                        <input type="radio"
                          id="blue"
                          name="colour"
                          value="blue">
                        <svg height="100"
                          width="100">
                          <circle cx="50"
                            cy="50"
                            r="20"
                            fill="blue">
                        </svg> 
                      </div>
                      <div class="form-comments">
                        <input type="radio"
                          id="yellow"
                          name="colour"
                          value="yellow">
                        <svg height="100"
                          width="100">
                          <circle cx="50"
                            cy="50"
                            r="20"
                            fill="yellow">
                        </svg> 
                      </div>
                    </div>
                    <div class="form-comments">
                      <input type="submit" value="Reply!">
                    </div>
                </form>
                    `)
          $(".form-inner-comments").on("submit", onReply);

        });
      }
    },
    error: function (xhr, ajaxOptions, thrownError) {
      console.log('error')

    }
  });
}